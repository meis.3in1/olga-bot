'use strict';
var colors = require('colors');
var discord = require('discord.js');
var fs = require('fs');
var http = require('http');
var https = require('https');
var jsonfile = require('jsonfile');

var CheckInChannel;
var LogChannel;
var QuestChannel;
var CheckInChannelID = "428488994708389888";
var LogChannelID = "435498947025567748";
var QuestChannelID = "428281068307873792";

var OwnerID = "346183575680450570";

var port = process.env.PORT || 8080;
var errorMessage = "Something is wrong, but I couldn't care less what.";
const client = new discord.Client();

setInterval(CheckIn, 600000);
setInterval(QuestStartReminder, 60000);
client.login('NDI4Mjc5NDUzMzk1ODQ1MTI0.DZwx2Q.YwhUGrVrHsX9p5u_0IijIbMFeCI');

http.createServer(function (req, res) {
    fs.readFile("Default.html", function (err, data) {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write(data);
        return res.end();
    });
}).listen(port);

client.on('ready', () => {
    client.user.setActivity('Questkeeper');
    SetChannels();

    ConsoleLogSingleEvent('Discord.js', `Logged in as ${client.user.username}`, colors.black, colors.bgWhite);
    ChatLogSingleEvent(bold(underline('Discord.js')), bold(underline(`Logged in as ${client.user.username}`)), CheckInChannel);

    //Component Initialization
    var Task = 'Component Initialization';

    var Quest = QuestInit();
    ConsoleLogSingleEvent(Task, Quest);
    var Scriber = SubscriptionsInit();
    ConsoleLogSingleEvent(Task, Scriber);

    ChatLogSingleEvent(Task, Quest, CheckInChannel);
});

client.on('message', msg => {
    if (msg.author == client.user) return;
    else if (msg.isMentioned(client.user)) {
        var Reply = "Don't ping me! :rolling_eyes:";

        msg.reply(Reply);
    }
    else if (msg.content.toLowerCase() === 'ping') {
        var Reply = ':ping_pong:';

        msg.channel.send(Reply);
    }
    else if (msg.content.toLowerCase() === 'quest') {
        var Reply = `Its ${bold(CurrentQuestNo() + '. ' + CurrentQuestSingle())}, right now.`;

        msg.channel.send(Reply);
    }
    else if (msg.content.toLowerCase() === 'q' || msg.content.toLowerCase() === 'quests') {
        var QuestQueue = CurrentQuestQueue();
        var Reply = bold(`[${QuestQueue.shift(0)}]`);

        for (var i = 1; i < QuestLength; i++) {
            Reply = `${Reply} > ${QuestQueue.shift(i)}`;
        }

        msg.channel.send(Reply);
    }
    else if (msg.content.toLowerCase() === 'subs') {
        if (msg.channel.id != QuestChannelID) return;
        var Reply;
        var records = ReportSingle(msg.author.toString());

        if (records) {
            Reply = `in my list:${codeblockMulti(records)}`;
        }
        else {
            Reply = `nope, you ain't in my list.`;
        }

        msg.reply(Reply);
    }
    else if (msg.content.toLowerCase().startsWith('sub')) {
        if (msg.channel.id != QuestChannelID) return;

        var quest = msg.content.toLowerCase().slice(4);

        if (quest.length > 1 && quest != 'resource') {
            var Reply = Subscribe(quest, msg.author.toString());

            if (Reply == null) {
                msg.channel.send(errorMessage);
            }
            else {
                ChatLogSingleEvent(`Subscription (+)`, `Total : ${Subscriptions.length} subscriptions.`, LogChannel);
                msg.reply(`${bold(quest)} quest, got it. :pencil:`);
            }
        }
    }
    else if (msg.content.toLowerCase() === 'unsub all') {
        if (msg.channel.id != QuestChannelID) return;
        var Reply;

        if (UnsubscribeAll(msg.author.toString())) {
            ChatLogSingleEvent(`Subscription (-)`, `Total : ${Subscriptions.length} subscriptions.`, LogChannel);
            Reply = `don't bother, done. :pencil:`;
        }
        else {
            Reply = `nope, you ain't in my list.`;
        }

        msg.reply(Reply);
    }
    else if (msg.content.toLowerCase().startsWith('unsub')) {
        if (msg.channel.id != QuestChannelID) return;
        var Reply = `${bold(quest)} quest, nope, that ain't in my list.`;

        var quest = msg.content.toLowerCase().slice(6);
        if (quest.length > 1) {
            if (Unsubscribe(quest, msg.author.toString())) {
                ChatLogSingleEvent(`Subscription (-)`, `Total : ${Subscriptions.length} subscriptions.`, LogChannel);
                Reply = `${bold(quest)} quest, don't bother, done. :pencil:`;
            }
        }

        msg.reply(Reply);
    }
    else if (msg.content.toLowerCase() === 'report') {
        if (msg.author.id != OwnerID) return;
        var Reply;
        var records = ReportAll();

        if (records) {
            Reply = `${underline("Olga's Record Book")}${codeblockMulti(records)}`;
        }
        else {
            Reply = `My record book is empty.`;
        }

        msg.channel.send(Reply);
    }
});

function SetChannels() {
    CheckInChannel = client.channels.get(CheckInChannelID);
    LogChannel = client.channels.get(LogChannelID);
    QuestChannel = client.channels.get(QuestChannelID);
}

function CheckIn() {
    https.get('https://olga-bot.herokuapp.com/');
    ChatLogSingleEvent('Check In', 'Online', CheckInChannel)
}

function QuestStartReminder() {
    var mins = new Date().getMinutes();
    if (mins == QuestBuffer) {
        var subscribers = '';
        var currentQuest = CurrentQuestSingle();

        if (currentQuest.toLowerCase() == 'resource') return;

        for (var i = 0; i < Subscriptions.length; i++) {
            if (Subscriptions[i].quest === currentQuest) {
                subscribers += Subscriptions[i].subscriber;
            }
        }

        ChatSendSingleMessage(`${subscribers} ${bold(currentQuest)} quest just started. Get to it.`, QuestChannel);
    }
}


//quester ================================================================================================
var QuestConfig;
var QuestEpoch;
var Quests;
var QuestLength;
var QuestBuffer;

function QuestInit() {
    try {
        QuestConfig = jsonfile.readFileSync('./QuestConfig.json');

        try {
            QuestEpoch = QuestConfig["QuestEpoch"];
            QuestBuffer = QuestConfig["QuestBuffer"];
            Quests = QuestConfig["Quests"];
            QuestLength = Quests.length;

            return `${__filename} : Success`;
        }
        catch (e) {
            return `${__filename} : ${e}`;
        }
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

function CurrentQuestNo() {
    try {
        var TimeNow = Math.floor(Date.now() / 1000);
        var HoursElasped = Math.floor((TimeNow - QuestEpoch) / 3600);

        if (HoursElasped > QuestLength - 1) {
            var Cycle = Math.floor(HoursElasped / QuestLength);
            var QuestNo = HoursElasped - (Cycle * QuestLength);

            return QuestNo;
        }
        else {
            var QuestNo = Math.floor(HoursElasped);

            return QuestNo;
        }
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

function CurrentQuestSingle() {
    try {
        return Quests[CurrentQuestNo()];
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

function CurrentQuestQueue() {
    try {
        var QuestQueue = [];

        QuestQueue.push(Quests[CurrentQuestNo()]);

        for (var i = parseInt(CurrentQuestNo() + 1); i < QuestLength; i++) {
            QuestQueue.push(Quests[i]);
        }

        for (var i = 0; i < (QuestLength - (QuestLength - CurrentQuestNo())); i++) {
            QuestQueue.push(Quests[i]);
        }

        return QuestQueue;
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}


//scriber =============================================================================================
var Subscriptions = [];
var SubscriptionsFile = './Subscriptions.json';

function SubscriptionsInit() {
    try {
        Subscriptions = jsonfile.readFileSync(SubscriptionsFile);

        return `${__filename} : Success`;
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

function Subscribe(quest, subscriber) {
    for (var i = 0; i < Quests.length; i++) {
        if (Quests[i].toLowerCase() === quest) {
            var sub = new Object;
            sub.quest = Quests[i];
            sub.subscriber = subscriber;

            Subscriptions.push(sub);
            UpdateSubscriptionsFile();
            return true;
        }
    }
}

function UnsubscribeAll(subscriber) {
    var newSubscriptions = [];
    var foundCount = 0;

    for (var i = 0; i < Subscriptions.length; i++) {
        if (Subscriptions[i].subscriber != subscriber) {
            newSubscriptions.push(Subscriptions[i]);
        }
        else {
            foundCount++;
        }
    }

    if (foundCount) {
        Subscriptions = newSubscriptions;
        UpdateSubscriptionsFile();
        return foundCount;
    }
}

function Unsubscribe(quest, subscriber) {
    var newSubscriptions = [];
    var foundCount = 0;

    for (var i = 0; i < Subscriptions.length; i++) {
        if (Subscriptions[i].subscriber != subscriber || Subscriptions[i].quest.toLowerCase() != quest) {
            newSubscriptions.push(Subscriptions[i]);
        }
        else {
            foundCount++;
        }
    }

    if (foundCount) {
        Subscriptions = newSubscriptions;
        UpdateSubscriptionsFile();
        return foundCount;
    }
}

function UpdateSubscriptionsFile() {
    jsonfile.writeFileSync(SubscriptionsFile, Subscriptions);
}

function ReportSingle(subscriber) {
    var foundRecords = '';

    for (var i = 0; i < Subscriptions.length; i++) {
        if (Subscriptions[i].subscriber === subscriber) {
            foundRecords = foundRecords + Subscriptions[i].quest + '\n';
        }
    }

    if (foundRecords.length) return foundRecords;
    else return false;
}

function ReportAll() {
    var records = '';

    for (var i = 0; i < Subscriptions.length; i++) {
        records = records + `${Subscriptions[i].subscriber} > ${Subscriptions[i].quest} + '\n`;
    }

    if (records.length) return records;
    else return false;
}


//messenger ==============================================================================================
function ConsoleLogSingleEvent(eventname, eventmessage) {
    console.log(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`);
}

function ConsoleLogSingleEventAdvanced(eventname, eventmessage, color, bg) {
    console.log(color(bg(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`)));
}

function ChatLogSingleEvent(eventname, eventmessage, channel) {
    channel.send(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`);
}

function ChatSendSingleMessage(message, channel) {
    channel.send(message);
}


//discordmarkdown=========================================================================================
function italic(message) {
    var text = "*" + message + "*";
    return text;
}

function bold(message) {
    var text = "**" + message + "**";
    return text;
}

function underline(message) {
    var text = "__" + message + "__";
    return text;
}

function strikethrough(message) {
    var text = "~~" + message + "~~";
    return text;
}

function codeblockSingle(message) {
    var text = "`" + message + "`";
    return text;
}

function codeblockMulti(message) {
    var text = "```" + message + "```";
    return text;
}

function codeblockMultiLang(language, message) {
    var text = "```" + language + "\n" + message + "```";
    return text;
}