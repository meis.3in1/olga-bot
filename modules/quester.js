﻿//requires
var fs = require('fs');
var jsonfile = require('jsonfile');
//quester.js
var exports = module.exports = {};

var QuestConfig;
var QuestEpoch;
var Quests;

var QuestLength;
exports.QuestLength = function () {
    return QuestLength;
}

var QuestBuffer;
exports.QuestBuffer = function () {
    return QuestBuffer;
}


exports.QuestInit = QuestInit;
function QuestInit() {
    try {
        QuestConfig = jsonfile.readFileSync('./QuestConfig.json');

        try {
            QuestEpoch = QuestConfig["QuestEpoch"];
            QuestBuffer = QuestConfig["QuestBuffer"];
            Quests = QuestConfig["Quests"];
            QuestLength = Quests.length;

            return `${__filename} : Success`;
        }
        catch (e) {
            return `${__filename} : ${e}`;
        }
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

exports.CurrentQuestNo = CurrentQuestNo;
function CurrentQuestNo() {
    try {
        var TimeNow = Math.floor(Date.now() / 1000);
        var HoursElasped = Math.floor((TimeNow - QuestEpoch) / 3600);

        if (HoursElasped > QuestLength - 1) {
            var Cycle = Math.floor(HoursElasped / QuestLength);
            var QuestNo = HoursElasped - (Cycle * QuestLength);

            return QuestNo;
        }
        else {
            var QuestNo = Math.floor(HoursElasped);

            return QuestNo;
        }
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

exports.CurrentQuestSingle = CurrentQuestSingle;
function CurrentQuestSingle() {
    try {
        return Quests[CurrentQuestNo()];
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}

exports.CurrentQuestQueue = CurrentQuestQueue;
function CurrentQuestQueue() {
    try {
        var QuestQueue = [];

        QuestQueue.push(Quests[CurrentQuestNo()]);

        for (var i = parseInt(CurrentQuestNo() + 1); i < QuestLength; i++) {
            QuestQueue.push(Quests[i]);
        }

        for (var i = 0; i < (QuestLength - (QuestLength - CurrentQuestNo())); i++) {
            QuestQueue.push(Quests[i]);
        }

        return QuestQueue;
    }
    catch (e) {
        return `${__filename} : ${e}`;
    }
}