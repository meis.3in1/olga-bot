//discordmarkdown.js
var exports = module.exports = {};

exports.ita = italic;
function italic(message) {
    var text = "*" + message + "*";
    return text;
}

exports.bol = bold;
function bold(message) {
    var text = "**" + message + "**";
    return text;
}

exports.und = underline;
function underline(message) {
    var text = "__" + message + "__";
    return text;
}

exports.str = strikethrough;
function strikethrough(message) {
    var text = "~~" + message + "~~";
    return text;
}

exports.codS = codeblockSingle;
function codeblockSingle(message) {
    var text = "`" + message + "`";
    return text;
}

exports.codM = codeblockMulti;
function codeblockMulti(message) {
    var text = "```" + message + "```";
    return text;
}

exports.codML = codeblockMultiLang;
function codeblockMultiLang(language, message) {
    var text = "```" + language + "\n" + message + "```";
    return text;
}