﻿//logger.js
var exports = module.exports = {};

exports.ConsoleLSE = ConsoleLogSingleEvent;
function ConsoleLogSingleEvent(eventname, eventmessage) {
    console.log(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`);
}

exports.ConsoleLSEA = ConsoleLogSingleEventAdvanced;
function ConsoleLogSingleEventAdvanced(eventname, eventmessage, color, bg) {
    console.log(color(bg(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`)));
}

exports.ChatLSE = ChatLogSingleEvent;
function ChatLogSingleEvent(eventname, eventmessage, channel) {
    channel.send(`${new Date().toUTCString()} : ${eventname} @ ${eventmessage}`);
}

exports.ChatSSM = ChatSendSingleMessage;
function ChatSendSingleMessage(message, channel) {
    channel.send(message);
}